# Tweeter

Tweeter is an interface allow you to test/play around with chuking message feature which have these following requiments

- Allow the user to input and send messages.
- Display the user's messages.
- If a user's input is less than or equal to 50 characters, post it as is.
- If a user's input is greater than 50 characters, split it into chunks that each is
less than or equal to 50 characters and post each chunk as a separate message.
- Messages will only be split on whitespace. If the message contains a span of
non-whitespace characters longer than 50 characters, display an error.
- Split messages will have a "part indicator" appended to the beginning of each
section. In the example above, the message was split into two chunks, so the part
indicators read "1/2" and "2/2". Be aware that these count toward the character
limit.

# Components

## Tweeter

The Basic chat box

### Example use

```ts
  <Tweeter
    messages={this.state.messageList}
    onMessageWasSent={this.onMessageWasSent}
  />
```

### Props

- messages: List of messages
- onMessageWasSent: Handle call when message is sent

# How to run

In the project directory, you can run:

### `npm install`

then

### `npm start`

To run the app

### Or

### `npm test`

To run the tests