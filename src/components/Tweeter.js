import * as React from 'react';
import TweeterModuleCss from './Tweeter.module.css';
import chatIconSvg from '../assets/icons/chat-icon.svg';
import closeIconPng from '../assets/icons/close-icon.png';
import { PopupWindow } from './PopupWindow';

export class Tweeter extends React.Component {
  constructor() {
    super()
    this.state = {
      close: true,
    }
    this.switch = this.switch.bind(this)
    this.onMessageWasSent = this.onMessageWasSent.bind(this)
  }

  render() {
    return <div className={TweeterModuleCss.container}>
      <div className={[TweeterModuleCss.tweeter,
          this.state.close ? '' : TweeterModuleCss.open ].join(' ')}
          onClick={this.switch}>
        <img 
          className={TweeterModuleCss.open_icon} 
          src={chatIconSvg}
        />
        <img 
          className={TweeterModuleCss.close_icon}
          src={closeIconPng}
        />
      </div>
      <PopupWindow 
        show={!this.state.close}
        messages={this.props.messages}
        onMessageWasSent={this.onMessageWasSent}
        onClose={this.switch}
      />
    </div>
  }

  switch() {
    this.setState({
      close: !this.state.close,
    })
  }

  onMessageWasSent(message) {
    this.props.onMessageWasSent(message)
  }
}