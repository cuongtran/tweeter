import * as React from 'react';
import { Message } from './Message';
import PopupWindowModuleCss from './PopupWindow.module.css';
import closeIconPng from '../assets/icons/close-icon.png';

export class PopupWindow extends React.Component {

  constructor() {
    super()
    this.state = {
      userClickOnTextBox: false,
      hasTextOnInput: false,
    }

    this.onInputTextClick = this.onInputTextClick.bind(this)
    this.onInputTextBlur = this.onInputTextBlur.bind(this)
    this.onKeyUp = this.onKeyUp.bind(this)
    this.onKeyDown = this.onKeyDown.bind(this)
    this.renderMessages = this.renderMessages.bind(this)
  }
  
  render() {
    return <div className={[PopupWindowModuleCss.container,
      this.props.show ? '' : PopupWindowModuleCss.hide].join(' ')}>
        <div className={PopupWindowModuleCss.header}>
          <div className={PopupWindowModuleCss.app_name}>
            Tweeter
          </div>
          <div className={PopupWindowModuleCss.close_button}>
            <img src={closeIconPng} onClick={this.props.onClose}/>
          </div>
        </div>
        <div className={PopupWindowModuleCss.body}>
          {this.renderMessages()}
        </div>
        <form className={[PopupWindowModuleCss.footer,
          this.state.userClickOnTextBox ? PopupWindowModuleCss.active : ''].join(' ')}>
          <div 
            className={PopupWindowModuleCss.input_text} 
            role="button"
            placeholder="Write your message here..."
            onClick={this.onInputTextClick}
            contentEditable="true"
            onBlur={this.onInputTextBlur}
            onKeyUp={this.onKeyUp}
            onKeyDown={this.onKeyDown}
          ></div>
        </form>
    </div>
  }

  onInputTextClick() {
    this.setState({
      userClickOnTextBox: true,
    })
  }

  onInputTextBlur() {
    this.setState({
      userClickOnTextBox: false,
    })
  }

  onKeyDown(event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      if (event.target.innerHTML !== "") {
        let message = event.target.innerHTML
        event.target.innerHTML = ""
        this.props.onMessageWasSent(message)
      }
    }
  }

  onKeyUp(event) {
    if (event.target.innerHTML.length > 0) {
      this.setState({
        hasTextOnInput: true,
      })
    } else {
      this.setState({
        hasTextOnInput: false,
      })
    }
  }

  renderMessages() {
    let listMessage = []
    for (const key in this.props.messages) {
      listMessage.push(
        <Message key={key}>{this.props.messages[key]}</Message>
      )
    }
    return listMessage;
  }
}