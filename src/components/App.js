import React, { Component } from 'react';
import { splitMessage } from '../utils/utils';
import './App.css';
import { Tweeter } from './Tweeter';

class App extends Component {
  constructor() {
    super()
    this.state = {
      messageList: [],
    }
    this.onMessageWasSent = this.onMessageWasSent.bind(this)
  }

  render() {
    return <div>
      <section className="section">
        <h1 className="title">Welcome to Tweeter</h1>
        <p className="body">Do you see the little button at the right bottom of the page? <br/>
        Click on it to begin!!!</p>
      </section>

      <Tweeter messages={this.state.messageList} onMessageWasSent={this.onMessageWasSent}></Tweeter>
    </div>;
  }

  onMessageWasSent(message) {
    let messages = []
    try {
      messages = splitMessage(message);
    } catch (e) {
      messages = [e.message]
    }
    
    this.setState({
      messageList: this.state.messageList.concat(messages)
    })
  }
}

export default App;
