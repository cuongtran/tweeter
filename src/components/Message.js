import * as React from 'react';
import MessageModuleCss from './Message.module.css';

export class Message extends React.Component {
  render() {
    return <div className={MessageModuleCss.message}>
      <div className={MessageModuleCss.message_content}>
        <div className={MessageModuleCss.message_text}>
          <span>{this.props.children}</span>
        </div>
      </div>
    </div>
  }
}