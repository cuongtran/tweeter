import { 
  splitMessage,
  noSpaceAndMoreThan,
  MessageError,
  getIndexes,
  getFirstMessage,
  LIMIT_CHARACTER
} from './utils';

describe('Test Regex', () => {
  it('should return Regex object with regex string', () => {
    expect(noSpaceAndMoreThan(LIMIT_CHARACTER)).toBeInstanceOf(RegExp);
    expect(noSpaceAndMoreThan(LIMIT_CHARACTER).toString()).toMatch("/\\S{50,}/g");
  })
})

describe('Test getIndexes function', () => {
  it('should return array of index of white space', () => {
    const message = "This message container";
    expect(getIndexes(message, " ")).toEqual(expect.arrayContaining([4,12]));
  })
})

describe('Test getFirstMessage function', () => {
  it('should cut the message at white space', () => {
    const message = "2/2 my messages, so I don't have to do it myself.";
  
    expect(message.length).toBeLessThanOrEqual(LIMIT_CHARACTER);
    expect(getFirstMessage(message, 5, " ")).toEqual("2/2");
  })
})

describe('Test splitMessage function', () => {
  it('should throw error when message contain no white space and more than 50 characters', () => {
    const message = "ThisIsAMessageThatContainNoWhiteSpaceAndMoreThan50Characters";
  
    expect(message.length).toBeGreaterThan(LIMIT_CHARACTER);
    // Match if string contain no space
    expect(message).toMatch(/\S/g);
    expect(() => {splitMessage(message)}).toThrowError(MessageError);
  })
  
  it('should throw error when message contain a word more than 50 characters', () => {
    const message = "ThisIsAMessageThatContainNoWhiteSpaceAndMoreThan50c haracters";
  
    let errorString = message.match(noSpaceAndMoreThan(LIMIT_CHARACTER))[0];
    expect(errorString.length).toBeGreaterThan(LIMIT_CHARACTER);
    expect(errorString).toMatch(/\S/g);
    expect(() => {splitMessage(message)}).toThrowError(MessageError);
  })
  
  it('should return the exact message if it is less than 50 characters', () => {
    const message = "This message container less than 50 characters";
  
    expect(message.length).toBeLessThan(LIMIT_CHARACTER);
    expect(splitMessage(message)).toEqual(expect.arrayContaining([message]));
  })
  
  it('should return the exact message if it have 50 characters', () => {
    const message = "This message container less than 50 characters tes";
  
    expect(message).toHaveLength(LIMIT_CHARACTER);
    expect(splitMessage(message)).toEqual(expect.arrayContaining([message]));
  })
  
  it('should return array of messages with each message is less than 50 character', () => {
    const message = "I can't believe Tweeter now supports chunking " +
                    "I can't believe Tweeter now supports chunking " +
                    "I can't believe Tweeter now supports chunking " +
                    "I can't believe Tweeter now supports chunking " +
                    "I can't believe Tweeter now supports chunking " +
                    "I can't believe Tweeter now supports chunking " +
                    "my messages, so I don't have to do it myself.";
  
    const arrayExpected = [
      "1/7 I can't believe Tweeter now supports chunking",
      "2/7 I can't believe Tweeter now supports chunking",
      "3/7 I can't believe Tweeter now supports chunking",
      "4/7 I can't believe Tweeter now supports chunking",
      "5/7 I can't believe Tweeter now supports chunking",
      "6/7 I can't believe Tweeter now supports chunking",
      "7/7 my messages, so I don't have to do it myself."
    ];

    let result = splitMessage(message);

    expect(message.length).toBeGreaterThan(LIMIT_CHARACTER);

    for (let message in result) {
      expect(message.length).toBeLessThanOrEqual(LIMIT_CHARACTER);
    }
  
    expect(result).toEqual(expect.arrayContaining(arrayExpected));
  })

  it('should remove begin and end space in message', () => {
    const message =  "  This is a test message   ";
    const ecpectedArray = ["This is a test message"];

    let result = splitMessage(message);
    expect(message.length).toBeLessThan(LIMIT_CHARACTER);
    expect(result).toEqual(expect.arrayContaining(ecpectedArray));
  })
})

