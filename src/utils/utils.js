export const LIMIT_CHARACTER = 50;

// Match string contain no white space and more than limited characters
export const noSpaceAndMoreThan = (min = 0) => {
  return new RegExp("\\S{" + min + ",}", "g");
}

export class MessageError extends Error {
  constructor() {
    super('Message should not contain any word more than 50 characters');
  }
}

export const splitMessage = (message, limitCharacter = 50) => {
  message = message.trim();
  if (message.match(noSpaceAndMoreThan(limitCharacter))) {
    throw new MessageError();
  }

  if (message.length <= limitCharacter) {
    return [message];
  }

  let tempMessage = message;
  let listChildMessage = [];
  const numberOfChildMessages = Math.floor(message.length / limitCharacter) + 1;
  for (let i = 1; i <= numberOfChildMessages; i++) {
    tempMessage = i + "/" + numberOfChildMessages + " " + tempMessage.trim();
    let childMessage = getFirstMessage(tempMessage, limitCharacter, " ");
    listChildMessage.push(childMessage);
    tempMessage = tempMessage.substr(childMessage.length);
    
  }
  return listChildMessage;
}

export const getFirstMessage = (message, limitCharacter, character) => {
  let childMessage = message;
  let listIndexOfWhiteSpace = getIndexes(message, character);
  for (let currentIndex = 0; currentIndex <= listIndexOfWhiteSpace.length; currentIndex++) {
    let tempMessage = message.substring(0, listIndexOfWhiteSpace[currentIndex]);
    if (tempMessage.length > limitCharacter) {
      break;
    }
    childMessage = tempMessage;
  }
  return childMessage;
}

export const getIndexes = (message, character) => {
  let listIndex = [];
  for (let currentIndex = 0; currentIndex < message.length; currentIndex++) {
    if (message[currentIndex] === character) {
      listIndex.push(currentIndex);
    }
  }
  return listIndex;
}